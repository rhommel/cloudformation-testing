#!/bin/bash


uniqueId=$(cat /dev/urandom | tr -dc 'a-zA-Z0-9' | fold -w 32 | head -n 1)

echo '{"Status":"SUCCESS","UniqueID":"'$uniqueId'","Data":"Some Data","Reason":"Just because I care"}'
